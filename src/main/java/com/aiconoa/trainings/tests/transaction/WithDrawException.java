package com.aiconoa.trainings.tests.transaction;

import javax.ejb.ApplicationException;

@ApplicationException(rollback=true)
public class WithDrawException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public WithDrawException() {
        super();
    }

    public WithDrawException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public WithDrawException(String message, Throwable cause) {
        super(message, cause);
    }

    public WithDrawException(String message) {
        super(message);
    }

    public WithDrawException(Throwable cause) {
        super(cause);
    }
    
    

}
