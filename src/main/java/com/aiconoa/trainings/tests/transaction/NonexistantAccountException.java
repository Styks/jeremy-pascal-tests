package com.aiconoa.trainings.tests.transaction;

import javax.ejb.ApplicationException;

@ApplicationException(rollback=true)
public class NonexistantAccountException extends RuntimeException{ /**
     * 
     */
    private static final long serialVersionUID = 1L;



}
