package com.aiconoa.trainings.tests.transaction;

import java.math.BigDecimal;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.aiconoa.trainings.tests.entity.Account;

@Stateless
public class TransactionService {
    @PersistenceContext(unitName="bankdb")
    private EntityManager em;
    
    public Account findAccountByNumber(int accountNumber) {
        try {
            return em.createQuery("SELECT a FROM Account a WHERE a.number =:number", Account.class)
                    .setParameter("number", accountNumber)
                    .getSingleResult();
        } catch (NoResultException noResultException) {
           throw new NonexistantAccountException();
        }
    }
    
    public void deposit(Account accountBeforeDeposit, BigDecimal amount) {
        Account accountBeforeDepositInSameTransaction=em.createQuery("SELECT a FROM Account a WHERE a.number =:number", Account.class)
                .setParameter("number", accountBeforeDeposit.getNumber())
                .getSingleResult();
        
        em.merge(new Account(accountBeforeDepositInSameTransaction.getId(),
                accountBeforeDepositInSameTransaction.getNumber(),
                amount.add(accountBeforeDepositInSameTransaction.getBalance()),accountBeforeDepositInSameTransaction.getVersion()));
    }

    public void withdraw(Account accountBeforeWithdraw, BigDecimal amount) {
        
        Account accountBeforeWithdrawInSameTransaction;
        try {
            accountBeforeWithdrawInSameTransaction = findAccountByNumber(accountBeforeWithdraw.getNumber());
        } catch (NonexistantAccountException NonexistantAccountException) {
           throw new WithDrawException("Compte inexistant");
        }
        
        if( accountBeforeWithdrawInSameTransaction.getBalance().subtract(amount).signum()==-1){
            throw new WithDrawException("à découvert!!");
        }
        
        em.merge(new Account(accountBeforeWithdrawInSameTransaction.getId(),
                accountBeforeWithdrawInSameTransaction.getNumber(),
                accountBeforeWithdrawInSameTransaction.getBalance().subtract(amount),accountBeforeWithdrawInSameTransaction.getVersion()));
        
        
    }


}
