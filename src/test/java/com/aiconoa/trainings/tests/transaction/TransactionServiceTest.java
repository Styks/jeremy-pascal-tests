package com.aiconoa.trainings.tests.transaction;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.ShouldMatchDataSet;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import com.aiconoa.trainings.tests.entity.Account;

@RunWith(Arquillian.class)
public class TransactionServiceTest {
    @PersistenceContext(unitName = "bankdb")
    private EntityManager em;
    @Resource
    private UserTransaction utx;

    @Inject
    private TransactionService transactionService;

    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClasses(TransactionService.class, Account.class,WithDrawException.class,NonexistantAccountException.class)
                .addAsResource("META-INF/persistence-test.xml", "META-INF/persistence.xml");
    }

    @Test
    public void transactionServiceExists() {
        assertNotNull(transactionService);
    }

    private Account findAccountByNumber(int accountNumber) {
        String jpql = "SELECT a FROM Account a WHERE a.number =:number";
        TypedQuery<Account> query = em.createQuery(jpql, Account.class);
        query.setParameter("number", accountNumber);
        return query.getSingleResult();
    }

    private Account createAccount(int accountNumber, BigDecimal amount)
            throws NotSupportedException, SystemException, IllegalStateException, SecurityException, HeuristicMixedException, HeuristicRollbackException, RollbackException {
        Account account = new Account(accountNumber, amount);
        // open transaction7
        utx.begin();
        em.persist(account);
        utx.commit();
        // close transaction
        return account;
    }

    private void removeAccount(int accountNumber)
            throws NotSupportedException, SystemException, IllegalStateException, SecurityException, HeuristicMixedException, HeuristicRollbackException, RollbackException {
        utx.begin();
        Account account = findAccountByNumber(accountNumber);
        em.remove(account);
        utx.commit();
    }

    @Test
    @UsingDataSet("datasets/accounts.yml")
    @ShouldMatchDataSet("datasets/expected-accounts.yml")
    public void depositMoneyIncreasesAccountBalance()
            throws IllegalStateException, SecurityException, NotSupportedException, SystemException, HeuristicMixedException, HeuristicRollbackException, RollbackException {
        Account accountBeforeDeposit = findAccountByNumber(1);
        transactionService.deposit(accountBeforeDeposit, new BigDecimal("11.34"));
        ;
    }

    @Test
    @UsingDataSet("datasets/accountsForWithdrawTest.yml")
    @ShouldMatchDataSet("datasets/expected-accountsForWithdrawTest.yml")
    public void withdrawMoneyDecreasesAccountBalance()
            throws IllegalStateException, SecurityException, NotSupportedException, SystemException, HeuristicMixedException, HeuristicRollbackException, RollbackException {
        Account accountBeforeWithdraw = findAccountByNumber(1);
        transactionService.withdraw(accountBeforeWithdraw, new BigDecimal("15.34"));
        ;
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    @Test
    @UsingDataSet("datasets/accountsForWithdrawTest.yml")
    public void negativeBalanceGeneratesOverdraftException(){
        thrown.expect(WithDrawException.class);
        Account accountTested = findAccountByNumber(1);
        transactionService.withdraw(accountTested, new BigDecimal("100.34"));
    }
    
    @Test
 //   @UsingDataSet("datasets/accountsForNonexistantAccountTest.yml")
    public void findNonexistantAccountGeneratesNonexistantAccountException(){
        thrown.expect(NonexistantAccountException.class);
        Account accountTested = transactionService.findAccountByNumber(99);
        
    }
    @Test
     public void findNonexistantAccountGeneratesWithDrawExceptionException(){
    thrown.expect(WithDrawException.class);
    Account accountTested2=new Account(1000, new BigDecimal("15.34"));
    transactionService.withdraw(accountTested2, new BigDecimal("100.34"));
    }
    // TODO: quid si on tente de faire un deposit <= 0 ?
    // TODO: règles métier: trop gros dépot interdits ? etc...
    // TODO:deux depots sur le meme compte, voir si les transactions ne se
    // mélangent pas

}
