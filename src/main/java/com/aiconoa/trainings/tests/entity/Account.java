package com.aiconoa.trainings.tests.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int number;
    private BigDecimal balance;
    @Version
    private Long version;

    public Account() {}

    public Account(int number, BigDecimal balance) {
        super();
        this.number = number;
        this.balance = balance;
    }

    public Account(long id, int number, BigDecimal balance, Long version) {
        super();
        this.number = number;
        this.balance = balance;
        this.id =id;
        this.version= version;
    }


    public long getId() {
        return id;
    }

    public int getNumber() {
        return number;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public Long getVersion() {
        return version;
    }

}
